package com.cureforward.eventcapture.service.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;

public class CreateEventRequest {

    @JsonProperty("uuid")
    private String uuid;

    @Size(min = 3, max = 50, message = "Category size should be between 3 and 50 characters")
    @JsonProperty("category")
    private String category;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
