package com.cureforward.eventcapture.service.response;

import com.cureforward.eventcapture.persistance.entity.Event;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventResponse {

    @JsonProperty("uuid")
    private String uuid;

    @JsonProperty("category")
    private String category;

    @JsonProperty("creationTime")
    private String creationTime;

    public EventResponse() { }

    public EventResponse(Event event) {
        this.uuid = event.getUuid().toString();
        this.category = event.getCategory();
        this.creationTime = event.getCreationTime().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }
}
