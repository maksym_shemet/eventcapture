package com.cureforward.eventcapture.service;

import com.cureforward.eventcapture.manager.EventManager;
import com.cureforward.eventcapture.service.request.CreateEventRequest;
import com.cureforward.eventcapture.service.response.EventResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping(value = "/events")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Validated //allows to validate Path and Request params; throws ConstraintViolationException
public class EventService {

    @Autowired
    private EventManager eventManager;

    @RequestMapping(method = RequestMethod.GET)
    public List<EventResponse> getAll(@RequestParam(value = "category", required = false) @Size(min = 1) String category) {
        return eventManager.getEvents(category);
    }

    @RequestMapping(path = "/{eventUuid}", method = RequestMethod.GET)
    public EventResponse get(@PathVariable("eventUuid") UUID uuid) {
        return eventManager.findByUuid(uuid);
    }


    @RequestMapping(method = RequestMethod.POST)
    public EventResponse save(@Valid @RequestBody CreateEventRequest createEventRequest) {
        return eventManager.save(createEventRequest);
    }

    @RequestMapping(path = "/{eventUuid}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("eventUuid") String uuid) {
        eventManager.delete(uuid);
    }
}
