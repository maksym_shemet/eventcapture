package com.cureforward.eventcapture.persistance.dao;

import com.cureforward.eventcapture.persistance.entity.Event;

import java.util.List;

public interface EventDao {

    List<Event> findAll();

    void save(Event event);

}
