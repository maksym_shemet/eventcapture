package com.cureforward.eventcapture.persistance.dao.impl;

import com.cureforward.eventcapture.persistance.dao.EventDao;
import com.cureforward.eventcapture.persistance.entity.Event;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EventDaoImpl implements EventDao {

    @Autowired
    private Cluster cluster;

    @Override
    public List<Event> findAll() {
        try(Session session = cluster.connect()) {
            List<Event> events = new ArrayList<>();
//            Mapper<Event> mapper = createMapper(session);
//            ResultSet resultSet = session.execute("SELECT * FROM events");
//            mapper.map(resultSet).forEach(x -> events.add(x));
            return events;
        }
    }

    @Override
    public void save(Event event) {
        try(Session session = cluster.connect()) {
//            Mapper<Event> mapper = createMapper(session);
//            mapper.save(event);
        }
    }

//    private Mapper<Event> createMapper(Session session) {
//        return new MappingManager(session).mapper(Event.class);
//    }
}
