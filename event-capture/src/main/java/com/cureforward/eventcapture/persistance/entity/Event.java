package com.cureforward.eventcapture.persistance.entity;

import com.datastax.driver.core.utils.UUIDs;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

@Table(value = "events")
public class Event {

    @PrimaryKey(value = "uuid")
    private UUID uuid = UUIDs.timeBased();

    @Column(value = "category")
    private String category;

    @Column(value = "creation_date")
    private LocalDateTime creationTime = LocalDateTime.now(Clock.systemUTC());

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }
}
