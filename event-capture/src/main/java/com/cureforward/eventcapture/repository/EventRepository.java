package com.cureforward.eventcapture.repository;

import com.cureforward.eventcapture.persistance.entity.Event;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

import java.util.List;
import java.util.UUID;

public interface EventRepository extends TypedIdCassandraRepository<Event, UUID> {

    //ALLOW FILTERING -> filtering rows on server side!
    @Query("select * from events where category = ?0 ALLOW FILTERING")
    List<Event> findByCategory(String category);
}
