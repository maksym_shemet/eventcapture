package com.cureforward.eventcapture.manager;

import com.cureforward.eventcapture.service.request.CreateEventRequest;
import com.cureforward.eventcapture.service.response.EventResponse;

import java.util.List;
import java.util.UUID;

public interface EventManager {

    List<EventResponse> getEvents(String category);

    EventResponse findByUuid(UUID uuid);

    EventResponse save(CreateEventRequest createEventRequest);

    void delete(String uuid);
}
