package com.cureforward.eventcapture.manager.impl;

import com.cureforward.eventcapture.manager.EventManager;
import com.cureforward.eventcapture.persistance.entity.Event;
import com.cureforward.eventcapture.repository.EventRepository;
import com.cureforward.eventcapture.service.request.CreateEventRequest;
import com.cureforward.eventcapture.service.response.EventResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class EventManagerImpl implements EventManager {

    @Autowired
    private EventRepository eventRepository;

    private List<EventResponse> findAll() {
        return StreamSupport.stream(eventRepository.findAll().spliterator(), false)
                .map(EventResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<EventResponse> getEvents(String category) {
        return Optional.ofNullable(category)
                .map(this::findByCategory)
                .orElseGet(this::findAll);
    }

    @Override
    public EventResponse save(CreateEventRequest createEventRequest) {
        Event event = new Event();
        event.setCategory(createEventRequest.getCategory());
        Optional.ofNullable(createEventRequest.getUuid()).ifPresent(x -> event.setUuid(UUID.fromString(x)));
        return new EventResponse(eventRepository.save(event));
    }

    @Override
    public EventResponse findByUuid(UUID uuid) {
        return new EventResponse(eventRepository.findOne(uuid));
    }

    public List<EventResponse> findByCategory(String category) {
        return eventRepository.findByCategory(category)
                .stream()
                .map(EventResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(String uuid) {
        eventRepository.delete(UUID.fromString(uuid));
    }
}
